// test/models/user.js

chai = require("chai");
truncate = require("../truncate");
userFactory = require("../factories/user");

UsersRepository = require("../../repositories/users.pg");
usersRepository = new UsersRepository();

describe("Users TDD", () => {
  let fakeUser = {};
  var expect = chai.expect;
  let id;

  beforeEach(async () => {
    fakeUser = {
      first_name: "test_first_name",
      last_name: "test_last_name",
      email: "test@test.com",
      password: "test",
      phone: "01 23 45 67 89",
      role: "Test",
    };
  });

  it("Create User", (done) => {
    usersRepository.signUp(fakeUser).then((response) => {
      newUser = response;
      id = newUser.result.id;
      expect(newUser.result.first_name).to.be.a("string");
      done();
    });

    // Check we can create a user

    // expect(newUser.result.first_name).to.equal(fakeUser.first_name);
    // expect(newUser.result.last_name).to.equal(fakeUser.last_name);
    // expect(newUser.result.email).to.equal(fakeUser.email);
    // expect(newUser.result.password).to.equal(fakeUser.password);
    // expect(newUser.result.phone).to.equal(fakeUser.phone);
    // expect(newUser.result.role).to.equal(fakeUser.role);

    // Check we can't duplicate user
    // newUser = await usersRepository.signUp(fakeUser);
    // expect(newUser.message).to.equal("User already exists");

    // Check we can't send anything to create user
    // newUser = await usersRepository.signUp("WTF");
    // expect(newUser.message).to.equal("Something went wrong");
  });

  it("Delete User", (done) => {
    // Check successful delete
    usersRepository.deleteUser(id).then((response) => {
      deleteUser = response;
      expect(deleteUser.message).to.equal("Successful user deletion");
      done();
    });

    // Check delete only if user exist
    // deleteUser = await usersRepository.deleteUser(999999999999999999);
    // expect(deleteUser.message).to.equal("User doesn't exist");
  });
});
